const {Sequelize} = require('sequelize');
const {MongoClient} = require('mongodb');


module.exports = (app) => {
    global.app = app;

    app.sequelize = new Sequelize(
        app.config.mysql.database,
        app.config.mysql.username,
        app.config.mysql.password,
        {
            host: app.config.mysql.host,
            dialect: 'mysql',
            logging: console.log,
            pool: {
                max: 150,
                min: 10,
                idle: 10000,
            },
            define: {
                timestamps: false,
            },
        },
    );


    app.mongose = new MongoClient(`mongodb://${app.config.mongodb.host}:${app.config.mongodb.port}/`);

    app.server = require('./server');
    app.helpers = require('../helpers');
    app.models = require('./models');
    // app.drivers = require('./drivers')

    app = {
        ...app,
        // api: require('./api'),
        // syncer: require('./syncer'),
        admin: require('./admin'),
        logs: require('./logs'),
        // brands: require('./brands'),
        // platforms: require('./platforms'),
        // patches: require('./patches'),
        // patches: require('./tools'),
        // destinations: require('./destinations'),
        // leads: require('./leads'),
        // sources: require('./sources'),
        // reports: require('./reports')
    };

    app.emitter.emit('start');
};
