const crypto = require('crypto');
const jsonwebtoken = require('jsonwebtoken');

app.helpers.apiEntity({
    name: 'manager',
    Model: app.models.Manager,
});

const login = (data) => {
  return new Promise((resolve, reject) => {
    if (!data.email || !data.password) {
      return reject('INVALID_LOGIN');
    }

    data.email = data.email.trim();

    let pwd = crypto
      .createHash('sha256')
      .update(data.password + app.config.crypto.salt)
      .digest('hex');

    app.models.Manager.findOne({
      where: {
        email: data.email,
        password: pwd,
      },
    })
      .then((result) => {
        if (!result) {
          return reject('INVALID_LOGIN');
        }

        result = result.dataValues;

        let now = Math.floor(Date.now() / 1000);

        result.jwt_token = jsonwebtoken.sign(
          JSON.stringify({
            id: result.id,
            role: result.role,
            created: now,
            expiration: now + 3600 * 24 * 30,
            rights: [],
          }),
          app.config.crypto.salt,
        );

        resolve(result);
      })
      .catch((err) => {
        app.logger.error('Admin: unable to get user from database', err);
        reject('SERVER_ERROR');
      });
  });
};

const getSelf = (jwt) => {
  return new Promise((resolve, reject) => {
    app.models.Manager.findOne({
      where: {
        id: jwt.id,
      },
    }).then((manager) => {
      manager = manager.dataValues;

      manager.rights = [];

      resolve(manager);
    });
  });
};

app.server.initRoutes({
  '/admin/auth': {
    post: {
      func: (data) => {
        console.log(data);
        return login(data);
      },
    },
  },
  '/admin/self': {
    get: {
      func: (data, jwt) => {
        return getSelf(jwt);
      },
      type: ['general'],
    },
  },
});
