// import {ObjectId} from "mongodb";
// import moment from 'moment';
const {ObjectId} = require('mongodb')

const mongodb = () => {

    // const client = await app.mongose.connect()
    //
    // const db = client.db('apple')
    // const collection = db.collection('logs');
    // const findResult = await collection.find({}).toArray()
    return new Promise(async (resolve, reject) => {
        const client = await app.mongose.connect()
        const db = client.db('nax')
        const collection = db.collection('logs');
        try {
            const findResult = await collection.find({status: 200}).limit(20).toArray()
            // console.log(findResult)
            resolve(findResult)
        } catch (e) {
            console.log('Server Error')
        }
    })

}

// const getBrandLogs =  (data, query, param) => {
//  return new Promise(async(resolve, reject) => {
//      const client = await app.mongose.connect()
//      const db = client.db(param.brand)
//      const collection = db.collection('logs');
//      try {
//          let startTime=new Date()
//          const findResult = await collection.find({request_id: '70bf9841-8317-45a0-a12b-f5de1297fa41'}).toArray()
//
//          let logId = findResult[0]._id
//          let bufferArray = logId.id
//          let last = bufferArray.length - 1
//          let min = bufferArray[last] - 10
//          let max = bufferArray[last] + 10
//
//          bufferArray[last] = min
//          let fromParse = JSON.parse(JSON.stringify(logId))
//
//          bufferArray[last] = max
//          let toParse = JSON.parse(JSON.stringify(logId))
//
//          let startFilter=new Date()
//          const findResult3 = await collection.find({$and: [{"_id": {$gte: ObjectId(fromParse)}}, {"_id": {$lte: ObjectId(toParse)}}]}).toArray()
//          console.log('findResult3', findResult3)
//          console.log('startTime', (new Date()-startTime)/1000 + 's')
//          console.log('startFilter', (new Date()-startFilter)/1000 + 's')
//
//          resolve(findResult3)
//      } catch (e) {
//          console.log('Server Error')
//      }
//
//  })
// }

const getBrandLogs = (data, query, param) => {
    return new Promise(async (resolve, reject) => {
        const client = await app.mongose.connect()
        const db = client.db(param.brand)
        const collection = db.collection('logs');
        try {
            let findResult = await collection.find({}).sort({_id: -1}).limit(200).toArray()
            findResult = app.helpers.createTimeLog(findResult)
            console.log(findResult)
            resolve(findResult)
        } catch (e) {
            resolve("SERVER_ERROR mongo")
            console.log('Server Error')
        }
    })
}

const filterLog = (data, query, param) => {
    return new Promise(async (resolve, reject) => {
        const client = await app.mongose.connect()
        const db = client.db(param.brand)
        const collection = db.collection('logs');
        let statusField = [];
        // console.log( typeof statusField)

        if (Array.isArray(data.status)) {
            statusField = data.status.map((el, index, arr) => parseInt(el));
        }
        // console.log(statusField)
        let aggregates = [{$match: {status: {$in: statusField}}}]

        try {
            let findResult = await collection.aggregate(aggregates).sort({_id: -1}).limit(200).toArray()
            findResult = app.helpers.createTimeLog(findResult)
            console.log(findResult)
            resolve(findResult)
        } catch (e) {
            resolve("SERVER_ERROR mongo")
            console.log('Server Error')
        }
    })
}

const searchLog = (data, query, param) => {
    return new Promise(async (resolve, reject) => {
        const client = await app.mongose.connect()
        const db = client.db(data.brand)
        const collection = db.collection('logs');

        let aggregates = [{$match: {['message.' + data.pathName]: data.query}}]
        try {
            let findResult = await collection.aggregate(aggregates).sort({_id: -1}).limit(200).toArray()
            findResult = app.helpers.createTimeLog(findResult)
            console.log(findResult)
            resolve(findResult)
        } catch (e) {
            resolve("SERVER_ERROR mongo")
            console.log('Server Error')
        }

    })
}

const hello = (data, params) => {
    return new Promise(async (resolve, reject) => {
        const client = await app.mongose.connect()
        const db = client.db(params.brand)
        const collection = db.collection('logs');
        let findResult = await collection.find({}).sort({_id: -1}).limit(200).toArray()
        // console.log(findResult)
        findResult = app.helpers.createTimeLog(findResult)
        // console.log(findResult)
        const from = app.helpers.fromTo(findResult).min
        const to = app.helpers.fromTo(findResult).max

        const condition = [{"_id": {$gte: ObjectId(from)}}, {"_id": {$lte: ObjectId(to)}}]

        const findResult3 = await collection.find({$and: condition}).toArray()
        // console.log('findResult3', findResult3)

        resolve(findResult3)
    })
}

app.server.initRoutes({
    // '/logs': {
    //     'post': {
    //         func(data, jwt, query, params) {
    //             return mongodb()
    //         },
    //         type: ['general']
    //     },
    // },
    '/log/:brand/logs': {
        'post': {
            func(data, jwt, query, params) {
                return getBrandLogs(data, query, params)
            },
            type: ['general']
        },
    },
    '/log/:brand/get_logs': {
        'post': {
            func(data, jwt, query, params, body) {
                return filterLog(data, query, params)
            },
            type: ['general']
        },
    },
    '/logs/search': {
        'post': {
            func(data, jwt, query, params) {
                return searchLog(data, query, params)
            },
            type: ['general']
        },
    },
    '/hello/:brand': {
        'post': {
            func(data, jwt, query, params) {
                return hello(data, params)
            },
            type: ['general']
        },
    },
    '/hello': {
        'post': {
            func(data, jwt, query, params) {
                return hello(data, params)
            },
            type: ['general']
        },
    },
    // '/logs/:brand': {
    //     'post': {
    //         func(data, jwt, query, params) {
    //             return mongodb()
    //         },
    //         type: ['general']
    //     },
    // },
    // '/logs/:brand/logs': {
    //     'post': {
    //         func(data, jwt, query, params) {
    //             console.log(data)
    //             console.log(params)
    //             console.log(query)
    //             return getBrandLogs(data,query, params)
    //         },
    //         type: ['general']
    //     },
    // },


});


// const from = app.helpers.fromTo(findResult).min
// const to = app.helpers.fromTo(findResult).max
//
// const condition = [{"_id": {$gte: ObjectId(from)}}, {"_id": {$lte: ObjectId(to)}}]
//
// const findResult3 = await collection.find({$and: condition}).toArray()
// console.log('findResult3', findResult3)

//db.products.find().sort({"created_at": 1}) --- 1 for asc and -1 for desc
//db.sales.find().sort({"date_field": 1}) -1
//db.collection.find().sort({ _id : -1 }).limit(10)