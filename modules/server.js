const fs = require('fs')
const http = require('http')
const https = require('https')
const jwt = require('jsonwebtoken')
const express = require('express')
const bodyParser = require('body-parser')


let srv = express()

srv.use(bodyParser.json({limit: '50mb', extended: true}))
srv.use(bodyParser.urlencoded({ extended: true }))

srv.use(function (req, res, next) {
    req.jwt = {}

    if (req.headers.authorization) {
        try {
            req.jwt = jwt.verify(req.headers.authorization, app.config.crypto.salt)
        } catch(e) {
            app.logger.debug(e)
        }

        res.set('Authorization', req.headers.authorization)
    }

    next()
})

srv.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'Authorization, Origin, X-Requested-With, Content-Type, Accept')
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE')
    res.header('Access-Control-Allow-Credentials', 'true')
    next()
})

// let privateKey  = fs.readFileSync(app.config.server.rest.key, 'utf8')
// let certificate = fs.readFileSync(app.config.server.rest.cert, 'utf8')
//
// let credentials = {key: privateKey, cert: certificate}
//
// let server = https.createServer(credentials, srv)
let server = http.createServer(srv)

server.listen(app.config.server.rest.port, function (err) {
    if (err) {
        return app.logger.error('REST Server: failed to start', err)
    }

    app.logger.info('REST Server: started on port '+app.config.server.rest.port)
})

let setAuth = function(req, res, data) {
    return new Promise(function (resolve, reject) {
        res.header('Authorization', data.jwt_token)

        resolve(data)
    })
}

let routes = {}

app.emitter.on('start', () => {
    for(let route in routes) {
        if (!routes.hasOwnProperty(route)) {
            continue;
        }

        for(let method in routes[route]) {
            if (!routes[route].hasOwnProperty(method)) {
                continue;
            }

            srv[method](route, function (req, res) {
                req.route = route;

                app.logger.debug('Method:', method, route)
                app.logger.debug('Body:', req.body)
                app.logger.debug('JWT:', req.jwt)
                app.logger.debug('Query:', req.query)
                app.logger.debug('Params:', req.params)

                let ip = req.headers['cf-connecting-ip'] || req.headers['x-real-ip'] || req.headers['x-forwarded-for'] || (req.connection.remoteAddress ? req.connection.remoteAddress.split('::ffff:').join('') : '')

                app.logger.debug('IP: ', ip)

                req.body._ip = ip

                // if (JSON.stringify(req.jwt) !== '{}' && app.config.owner !== req.jwt.brand) {
                //     app.logger.debug('Response (JWT brand doesn\'t match):', {
                //         status: 403,
                //         data: 'FORBIDDEN'
                //     })
                // }

                if (!req.jwt.rights || req.jwt.rights.indexOf('superadmin') === -1) {
                    if (app.config.server.rest.ip_whitelist && routes[route][method].type && routes[route][method].type.indexOf('auth') === -1 && routes[route][method].type.indexOf('file') === -1 && routes[route][method].type.indexOf('affiliate') === -1) {
                        if (whitelist.indexOf(ip) === -1 && whitelist.indexOf('127.0.0.1') === -1) {
                            app.logger.debug('Response (IP is not whitelisted): ', {
                                status: 403,
                                data: 'FORBIDDEN'
                            })
                            return res.send({
                                status: 403,
                                error: 'FORBIDDEN'
                            })
                        }
                    }
                } else {
                    if (JSON.stringify(req.jwt) !== '{}' && app.config.owner !== req.jwt.brand) {
                        app.logger.debug('Response (JWT brand doesn\'t match):', {
                            status: 403,
                            data: 'FORBIDDEN'
                        })
                        return res.send({
                            status: 403,
                            error: 'FORBIDDEN'
                        })
                    }
                }

                let process = function(route, method, req, res) {
                    routes[route][method].func(req.body, req.jwt, req.query, req.params)
                        .then(result => {
                            if (routes[route][method].resolve) {
                                let resolveFunc = function(i, resolveFuncs, result) {
                                    if (routes[route][method].resolve[i]) {
                                        routes[route][method].resolve[i](req, res, result)
                                            .then(result => {
                                                resolveFunc(i+1, resolveFuncs, result)
                                            })
                                    } else {
                                        app.logger.debug('Response: ', {
                                            status: 200,
                                            data: result
                                        })
                                        res.send({
                                            status: 200,
                                            data: result
                                        })
                                    }
                                }

                                resolveFunc(0, routes[route][method].resolve, result)
                            } else {
                                app.logger.debug('Response: ', result._plain || {
                                    status: 200,
                                    data: result
                                })
                                res.send(result._plain || {
                                    status: 200,
                                    data: result
                                })
                            }
                        })
                        .catch(error => {
                            app.logger.debug('Response: ', {
                                status: 400,
                                error: error
                            })
                            res.send({
                                status: 400,
                                error: error
                            })
                        })
                }

                if (routes[route][method].type && routes[route][method].type.indexOf('auth') !== -1) {
                    if (!req.jwt || !req.jwt.id) {
                        app.logger.debug('Response: ', {
                            status: 403,
                            data: 'FORBIDDEN'
                        })
                        return res.send({
                            status: 403,
                            error: 'FORBIDDEN'
                        })
                    }
                }

                if (routes[route][method].type && routes[route][method].type.indexOf('manager') !== -1) {
                    if (!req.jwt || !req.jwt.role || req.jwt.role < 2) {
                        app.logger.debug('Response: ', {
                            status: 403,
                            data: 'FORBIDDEN'
                        })
                        return res.send({
                            status: 403,
                            error: 'FORBIDDEN'
                        })
                    }
                } else {
                    process(route, method, req, res)
                    return
                }

                if (routes[route][method].type && routes[route][method].type.indexOf('admin') !== -1) {
                    if (!req.jwt || !req.jwt.role || req.jwt.role < 3) {
                        app.logger.debug('Response: ', {
                            status: 403,
                            data: 'FORBIDDEN'
                        })
                        return res.send({
                            status: 403,
                            error: 'FORBIDDEN'
                        })
                    }
                } else {
                    process(route, method, req, res)
                }
            })
        }
    }

    srv.get('*', function(req, res) {
        res.send({
            status: 404,
            error: 'NOT_FOUND'
        })
    })

    srv.post('*', function(req, res) {
        res.send({
            status: 404,
            error: 'NOT_FOUND'
        })
    })

    srv.put('*', function(req, res) {
        res.send({
            status: 404,
            error: 'NOT_FOUND'
        })
    })

    srv.delete('*', function(req, res) {
        res.send({
            status: 404,
            error: 'NOT_FOUND'
        })
    })
})

const initRoutes = conf => {
    for(let path in conf) {
        if (!conf.hasOwnProperty(path)) {
            continue;
        }

        let route = conf[path]

        if (routes[path] === undefined) {
            routes[path] = {}
        }

        for (let method in route) {
            if (!route.hasOwnProperty(method)) {
                continue
            }

            if (routes[path][method] === undefined) {
                routes[path][method] = {}
            }

            if (route[method].func !== undefined) {
                routes[path][method].func = route[method].func
            }

            if (route[method].type !== undefined) {
                routes[path][method].type = route[method].type
            }

            if (route[method].resolve !== undefined) {
                if (!Array.isArray(route[method].resolve)) {
                    route[method].resolve = [route[method].resolve]
                }

                if (routes[path][method].resolve === undefined) {
                    routes[path][method].resolve = []
                }

                for(let i=0, l=route[method].resolve.length; i<l; i++) {
                    routes[path][method].resolve.push(route[method].resolve[i])
                }
            }
        }
    }
}

initRoutes({
    '/auth': {
        'post': {
            resolve: setAuth
        }
    }
})

module.exports = {
    initRoutes
}
