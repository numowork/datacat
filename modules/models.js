const {Model, DataTypes} = require('sequelize');

class Manager extends Model {
}

Manager.init(
    {
        id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
        password: DataTypes.STRING,
        fullname: DataTypes.STRING,
        email: DataTypes.STRING,
        created: DataTypes.INTEGER,
        online: DataTypes.INTEGER,
        last_online: DataTypes.INTEGER,
        role: DataTypes.INTEGER,
        invalidate_token_time: DataTypes.INTEGER,
    },
    {
        sequelize: app.sequelize,
        modelName: 'manager',
        indexes: [{unique: true, fields: ['email']}],
    },
);

class Brand extends Model {
}

Brand.init(
    {
        name: {type: DataTypes.STRING, primaryKey: true},
        company_name: DataTypes.STRING,
        ip: DataTypes.STRING,
        domain: DataTypes.STRING,
        provider: DataTypes.STRING,
    },
    {
        sequelize: app.sequelize,
        modelName: 'brand',
    },
);

class Platform extends Model {
}

Platform.init(
    {
        name: {type: DataTypes.STRING, primaryKey: true},
        company_name: DataTypes.STRING,
        ip: DataTypes.STRING,
        domain: DataTypes.STRING,
        provider: DataTypes.STRING,
    },
    {
        sequelize: app.sequelize,
        modelName: 'platform'
    },
);

class Patch extends Model {
}

Patch.init(
    {
        id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
        version: DataTypes.STRING,
        patch: DataTypes.STRING,
    },
    {
        sequelize: app.sequelize,
        modelName: 'patch'
    },
);

module.exports = {
    Manager,
    Brand,
    Platform,
    Patch
};
