const fs = require('fs'),
  EventEmitter = require('events'),
  log4js = require('log4js');

let app = {
  config: JSON.parse(fs.readFileSync('config.json', 'utf8')),
  emitter: new EventEmitter(),
  logger: log4js.getLogger(),
};

app.logger.level = app.config.log;

require(__dirname + '/modules/main')(app);

process.on('uncaughtException', function (err) {
  app.logger.error('Uncaught exception: ', err);
});
