const crypto = require('crypto');
const fs = require('fs');
const {Op} = require('sequelize');
const {exec} = require('child_process')
const path = require("path");
const moment = require("moment");

const getTable = (Model, filters, where) => {
    return new Promise((resolve, reject) => {
        if (!filters) {
            filters = {};
        }

        if (filters.limit !== undefined) {
            filters.limit = parseInt(filters.limit);
        }

        if (filters.offset !== undefined) {
            filters.offset = parseInt(filters.offset);
        }

        if (filters.order !== undefined) {
            if (filters.order[0].includes('.')) {
                let k = filters.order[0].split('.');
                let OrderModel = undefined;
                for (let i = 0, l = Model.length; i < l; i++) {
                    if (Model[i].as === k[0]) {
                        OrderModel = Model[i];
                    }
                }
                filters.order = [[OrderModel, k[1], filters.order[1]]];
                // filters.order = [k[0]+'s.'+k[1], filters.order[1]]
            } else {
                filters.order = [filters.order];
            }
        }

        let whereFilters = [];

        if (where) {
            for (let n in where) {
                if (!where.hasOwnProperty(n)) {
                    continue;
                }

                let key = n;
                if (n.includes('.')) {
                    key = '$' + n + '$';
                }

                whereFilters.push({
                    [key]: where[n],
                });
            }
        }

        let filterTypes = ['like', 'ne', 'in', 'lte', 'gte', 'eq'];

        for (let f in filterTypes) {
            if (!filterTypes.hasOwnProperty(f)) {
                continue;
            }

            let filter = filterTypes[f];

            for (let n in filters.where) {
                if (!filters.where.hasOwnProperty(n)) {
                    continue;
                }

                let key = n;
                if (n.includes('.')) {
                    key = '$' + n + '$';
                }

                if (filters.where[n]['$' + filter] !== undefined) {
                    if (Array.isArray(filters.where[n]['$' + filter])) {
                        let orFilter = [];
                        for (let i in filters.where[n]['$' + filter]) {
                            if (!filters.where[n]['$' + filter].hasOwnProperty(i)) {
                                continue;
                            }

                            if (filters.where[n]['$' + filter][i].includes('&')) {
                                let fields = filters.where[n]['$' + filter][i].split('&'),
                                    ands = [];

                                for (let f in fields) {
                                    let val = fields[f].split('=');

                                    if (val.length > 1 && val[0].charAt(0) === '$') {
                                        val = {
                                            [Op[val[0].slice(1)]]: val[1],
                                        };
                                    }

                                    ands.push(val);
                                }

                                orFilter.push({
                                    [Op.and]: ands,
                                });
                            } else {
                                let val = filters.where[n]['$' + filter][i].split('=');

                                if (val.length > 1 && val[0].charAt(0) === '$') {
                                    val = {
                                        [Op[val[0].slice(1)]]: val[1],
                                    };
                                }

                                orFilter.push(val);
                            }
                        }

                        whereFilters.push({
                            [key]: {
                                [Op.or]: orFilter,
                                // [Op.and]: andFilter
                            },
                        });
                    } else {
                        if (filters.where[n]['$' + filter].includes('&')) {
                            let fields = filters.where[n]['$' + filter].split('&'),
                                ands = [];

                            for (let f in fields) {
                                let val = fields[f].split('=');

                                if (val.length > 1 && val[0].charAt(0) === '$') {
                                    val = {
                                        [Op[val[0].slice(1)]]: val[1],
                                    };
                                }

                                ands.push(val);
                            }

                            whereFilters.push({
                                [key]: {
                                    [Op.and]: ands,
                                },
                            });
                        } else {
                            let val = filters.where[n]['$' + filter].split('=');

                            if (val.length > 1 && val[0].charAt(0) === '$') {
                                whereFilters.push({
                                    [key]: {
                                        [Op[val[0].slice(1)]]: val[1],
                                    },
                                });
                            } else {
                                whereFilters.push({
                                    [key]: {
                                        [Op[filter]]: filters.where[n]['$' + filter],
                                    },
                                });
                            }
                        }
                    }
                }
            }
        }

        filters.where = whereFilters;

        // filters.logging = console.logs

        if (Array.isArray(Model)) {
            let M = Model.shift();

            filters.include = Model;

            M.findAndCountAll(filters).then(resolve).catch(reject);
        } else {
            Model.findAndCountAll(filters).then(resolve).catch(reject);
        }
    });
};

const postProcessor = (field, processor) => {
    if (typeof processor === 'object') {
        let reverseMap = {};

        for (let n in processor) {
            reverseMap[processor[n]] = n;
        }

        return reverseMap[field];
    }

    switch (processor) {
        case 'time':
            let date = new Date(field * 1000).toISOString().split('T');
            return date[0] + ' ' + date[1].split('.000Z')[0];
        default:
            return field;
    }
};

const downloadTable = (arguments, func, postProcess = {}) => {
    return new Promise((resolve, reject) => {
        arguments[2].offset = 0;
        arguments[2].limit = 10000;

        let fields = arguments[2].file_fields;

        func(...arguments)
            .then((res) => {
                if (!res.rows) {
                    res.rows = [];
                }

                let fileContent, row, fileName;

                switch (arguments[2].file) {
                    case 'csv':
                        fileContent = [];

                        row = [];
                        for (let n in fields) {
                            row.push(fields[n]);
                        }

                        fileContent.push(row.join(','));

                        for (let n in res.rows) {
                            let row = [];
                            for (let m in fields) {
                                let f = fields[m],
                                    toks = f.split('.'),
                                    val = f.includes('.')
                                        ? res.rows[n][toks[0]]
                                            ? res.rows[n][toks[0]].dataValues[toks[1]]
                                            : ''
                                        : res.rows[n][f];

                                row.push(postProcess[f] !== undefined ? postProcessor(val, postProcess[f]) : val);
                            }

                            fileContent.push(row.join(','));
                        }

                        fileName =
                            crypto
                                .createHash('sha256')
                                .update(Date.now() + app.config.crypto.salt)
                                .digest('hex')
                                .slice(0, 16) + '.csv';

                        fs.writeFileSync(app.config.upload.dir + fileName, fileContent.join('\n'));

                        resolve({
                            file: fileName,
                        });

                        break;
                    case 'xls':
                        fileContent = [];

                        row = [];
                        for (let n in fields) {
                            row.push(fields[n]);
                        }

                        fileContent.push(row);

                        for (let n in res.rows) {
                            let row = [];
                            for (let m in fields) {
                                let f = fields[m],
                                    toks = f.split('.'),
                                    val = f.includes('.')
                                        ? res.rows[n][toks[0]]
                                            ? res.rows[n][toks[0]].dataValues[toks[1]]
                                            : ''
                                        : res.rows[n][f];

                                row.push(postProcess[f] !== undefined ? postProcessor(val, postProcess[f]) : val);
                            }

                            fileContent.push(row);
                        }

                        let wb = xl.utils.book_new(),
                            ws = xl.utils.aoa_to_sheet(fileContent);

                        xl.utils.book_append_sheet(wb, ws, 'Report');

                        fileName =
                            crypto
                                .createHash('sha256')
                                .update(Date.now() + app.config.crypto.salt)
                                .digest('hex')
                                .slice(0, 16) + '.xlsx';

                        xl.writeFile(wb, app.config.upload.dir + fileName);

                        resolve({
                            file: fileName,
                        });

                        break;
                }
            })
            .catch(reject);
    });
};

const apiEntity = ({
                       name,
                       Model,
                       validator = (data) => {
                           return new Promise((resolve) => {
                               resolve(data);
                           });
                       },
                       mixins = {},
                       updateFields = [],


                   }) => {
    const getAll = (data, jwt, filters) => {
        return new Promise((resolve, reject) => {
            getTable(Model, filters)
                .then(resolve)
                .catch((err) => {
                    app.logger.error(`${name}: get table get error`, err);
                    reject('SERVER_ERROR');
                });
        });
    };

    const add = (data) => {
        return new Promise((resolve, reject) => {
            let key = Model.primaryKeyAttribute || 'id'
            validator(data)
                .then((entity) => {
                    Model.findOne({
                        where: {
                            [key]: data[key],
                        },
                    })
                        .then((result) => {
                            if (result) {
                                return reject(data[key].toUpperCase() + '_EXISTS');
                            }

                            Model.create(entity)
                                .then(() => {
                                    resolve(entity);
                                })
                                .catch((err) => {
                                    app.logger.error(`${name}: add error`, err);
                                    reject('SERVER_ERROR');
                                });

                            resolve(entity);
                        })
                        .catch((err) => {
                            app.logger.error(`${name}: get entity for update error`, err);
                            reject('SERVER_ERROR');
                        });
                })
                .catch(reject);
        });
    };

    const get = (params) => {
        console.log(params[name])
        return new Promise((resolve, reject) => {
            if (!params[name]) {
                return reject('INVALID_' + name.toUpperCase());
            }

            Model.findOne({
                where: {
                    [Model.primaryKeyAttribute || 'id']: params[name],
                },
            })
                .then((entity) => {
                    if (!entity) {
                        return reject('INVALID_' + name.toUpperCase());
                    }

                    entity = entity.dataValues;

                    resolve(entity);
                })
                .catch((err) => {
                    app.logger.error(`${name}: get entity for update error`, err);
                    reject('SERVER_ERROR');
                });
        });
    };

    const update = (data, params) => {
        return new Promise((resolve, reject) => {
            if (!params[name]) {
                return reject('INVALID_' + name.toUpperCase());
            }

            Model.findOne({
                where: {
                    [Model.primaryKeyAttribute || 'id']: params[name],
                },
            })
                .then((entity) => {
                    if (!entity) {
                        return reject('INVALID_' + name.toUpperCase());
                    }

                    entity = entity.dataValues;

                    for (let i = 0, l = updateFields.length; i < l; i++) {
                        if (data[updateFields[i]] === undefined) {
                            continue;
                        }
                        entity[updateFields[i]] = data[updateFields[i]];
                    }

                    Model.update(entity, {
                        where: {
                            [Model.primaryKeyAttribute || 'id']: params[name],
                        },
                    })
                        .then(() => {
                            resolve(entity);
                        })
                        .catch((err) => {
                            app.logger.error(`${name}: update error`, err);
                            reject('SERVER_ERROR');
                        });
                })
                .catch((err) => {
                    app.logger.error(`${name}: get entity for update error`, err);
                    reject('SERVER_ERROR');
                });
        });
    };

    const remove = (params) => {
        return new Promise((resolve, reject) => {
            if (!params[name]) {
                return reject('INVALID_' + name.toUpperCase());
            }

            Model.findOne({
                where: {
                    [Model.primaryKeyAttribute || 'id']: params[name],
                },
            })
                .then((_old) => {
                    if (!_old) {
                        return reject('INVALID_' + name.toUpperCase());
                    }

                    _old = _old.dataValues;

                    Model.destroy({
                        where: {
                            [Model.primaryKeyAttribute || 'id']: params[name],
                        },
                    })
                        .then((result) => {
                            resolve({_old});
                        })
                        .catch((err) => {
                            app.logger.error(`${name}: remove error`, err);
                            reject('SERVER_ERROR');
                        });
                })
                .catch((err) => {
                    app.logger.error(`${name}: get entity for update error`, err);
                    reject('SERVER_ERROR');
                });
        });
    };

    let routes = {
        [`/${name}s/all`]: {
            get: {
                func(data, jwt, query) {
                    return getAll(data, jwt, query);
                },
                type: [name + 's'],
            },
            post: {
                func(data) {
                    return add(data);
                },
                type: [name + 's'],
            },
        },
        [`/${name}s/all/download`]: {
            get: {
                func(data, jwt, query, params) {
                    return downloadTable([data, jwt, query, params], get);
                },
                type: [name + 's'],
            },
        },
        [`/${name}/:${name}`]: {
            get: {
                func: (data, jwt, query, params) => {
                    return get(params);
                },
                type: [name + 's'],
            },
            put: {
                func: (data, jwt, query, params) => {
                    return update(data, params);
                },
                type: [name + 's'],
            },
            delete: {
                func: (data, jwt, query, params) => {
                    return remove(params);
                },
                type: [name + 's'],
            },
        },
    };

    for (let n in mixins) {
        if (routes[n] === undefined) {
            routes[n] = mixins[n];
            continue;
        }

        for (let m in mixins[n]) {
            if (routes[n][m] === undefined) {
                routes[n][m] = mixins[n][m];
                continue;
            }

            for (let o in mixins[n][m]) {
                routes[n][m][o] = mixins[n][m][o];
            }
        }
    }

    app.server.initRoutes(routes);
};


const callScript = (name, params, flag = true) => {
    return new Promise((resolve, reject) => {
        // const exists = fs.existsSync(`~/flag.text`,);
        // if (exists) {
        //     return reject("SCRIPT_WORKING")
        // }

        // fs.writeFileSync(`~/flag.text`, 'script working', 'utf8');


        let scriptStr = []

        for (let param in params) {
            scriptStr.push('PARAM_' + param.toUpperCase() + '="' + params[param] + '"')
        }

        let log = __dirname + '/logs/' + app.helpers.uuidv4() + '.log'

        scriptStr = scriptStr.join(' ') + ' ' + __dirname + '/deploy/' + name + '.sh > ' + log
        console.log(log)
        console.log(scriptStr)
        const resolveFunction = () => {
            exec(scriptStr, (error, stdout, stderr) => {
                console.log('log1', error)
                console.log('log2', stdout)
                console.log('log3', stderr)
                if (error) {
                    // fs.unlinkSync(`~/flag.text`);
                    return reject(error)
                }
                if (!flag) {
                    // fs.unlinkSync(`~/flag.text`);
                    resolve(stdout)
                }
            })
        }

        if (!flag) {
            resolveFunction()
            return
        }

        setTimeout(() => {
            resolveFunction()

        }, 1)

        app.logger.error('13')
        // fs.unlinkSync(`~/flag.text`);
        resolve(log)

    })
}


const allBrandsArray = (data) => {
    let array = ''
    let brandRoot = ''
    for (let i = 0; i < data.length; i++) {
        array += `["${data[i].name}"]="root@${data[i].ip} -i ~/All/ssh/root.pem -o StrictHostKeyChecking=no"\n`
        brandRoot += `["${data[i].name}"]="root@${data[i].ip}"\n`
    }
    return {array, brandRoot}
}

const fromTo = (data) => {
    let logId = data[0]._id
    let bufferArray = logId.id
    let last = bufferArray.length - 1
    let min = bufferArray[last] - 10
    let max = bufferArray[last] + 10

    bufferArray[last] = min
    let fromParse = JSON.parse(JSON.stringify(logId))

    bufferArray[last] = max
    let toParse = JSON.parse(JSON.stringify(logId))
    return {min: fromParse, max: toParse}
}

const uuidv4 = () => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        let r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8)
        return v.toString(16)
    })
}

String.prototype.replaceAt = function (index, replacement) {
    return this.substring(0, index) + replacement + this.substring(index + replacement.length);
}
//currentTime

const createTimeLog = (arrayLogs) => {
    for (let i = 0; i < arrayLogs.length; i++) {
        const timestamp = arrayLogs[i]._id.getTimestamp()
        const time = moment(timestamp).format('DD MM YYYY, h:mm:ss a');
        arrayLogs[i].createTime = time
    }
    return arrayLogs
}


module.exports = {
    apiEntity,
    callScript,
    allBrandsArray,
    fromTo,
    uuidv4,
    createTimeLog
};
