DROP TABLE IF EXISTS `managers`;
CREATE TABLE `managers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(255) DEFAULT '',
  `fullname` varchar(255) DEFAULT '',
  `email` varchar(255) NOT NULL,
  `created` int(11) DEFAULT '0',
  `online` tinyint(1) DEFAULT '0',
  `last_online` int(11) DEFAULT '0',
  `role` tinyint(1) DEFAULT '1',
  `invalidate_token_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE UNIQUE INDEX manager ON `managers`(`email`);

INSERT INTO `managers` (`id`, `fullname`, `email`, `password`, `created`, `role`) VALUES
(1,	 'Admin Admin',	'admin@admin.com',	'5f1e9cbc37d671ecf7d94724e8aec3729fab1988aa1cba6a8143b37d59e708c0',	1562327644, 10);

/* Brand */
DROP TABLE IF EXISTS `brands`;
CREATE TABLE `brands` (
  `name` varchar(255) NOT NULL,
  `company_name` varchar(255) DEFAULT '',
  `ip` varchar(255) NOT NULL,
  `domain` varchar(255) NOT NULL,
  `provider` varchar(255) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `brands` (`name`, `company_name`, `ip`, `domain`, `provider`) VALUES
('MacBook Pro',	'Apple', '127.0.0.1', 'apple.com', 'SharkSoft');

/* Platform */
DROP TABLE IF EXISTS `platforms`;
CREATE TABLE `platforms` (
  `name` varchar(255) NOT NULL,
  `company_name` varchar(255) DEFAULT '',
  `ip` varchar(255) NOT NULL,
  `domain` varchar(255) NOT NULL,
  `provider` varchar(255) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `platforms` (`name`, `company_name`, `ip`, `domain`, `provider`) VALUES
('MacBook Pro',	'Apple', '127.0.0.1', 'apple.com', 'SharkSoft');

