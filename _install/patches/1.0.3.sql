/* Patch */
DROP TABLE IF EXISTS `patches`;
CREATE TABLE `patches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(255) DEFAULT '',
  `patch` varchar(30000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `patches` (`id`, `version`, `patch`) VALUES
('1',	'1.0.2', '111');